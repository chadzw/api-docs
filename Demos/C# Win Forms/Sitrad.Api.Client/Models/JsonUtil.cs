﻿using System.IO;
using System.Text;
using System.Text.Json;

namespace Sitrad.Api.Client.Models
{
    public static class JsonUtil
    {
        public static string BeautifyJson(string json)
        {
            try
            {
                using var document = JsonDocument.Parse(json);
                using var stream = new MemoryStream();
                using var writer = new Utf8JsonWriter(stream, new JsonWriterOptions() { Indented = true });
                document.WriteTo(writer);
                writer.Flush();
                return Encoding.UTF8.GetString(stream.ToArray());
            }
            catch
            {
                return json;
            }
        }
    }
}
