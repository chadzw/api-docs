﻿
namespace Sitrad.Api.Client.Models
{
    public class ServerConfiguration
    {
        public string SitradApiUrl { get; set; } = "https://fgserver-pro.sitrad.com/api/v1/";

        public string User { get; set; } = "test";

        public string Password { get; set; } = "test";

        public bool IgnoreCertificateValidation { get; set; } = true;
    }
}
