﻿using System.Collections.Generic;
using System.Net;

namespace Sitrad.Api.Client.Models
{
    public class Request
    {
        public HttpStatusCode StatusCode { get; set; }
        public List<string> Errors { get; set; } = new List<string>();
        public string Body { get; set; }
    }
}
