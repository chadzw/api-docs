﻿using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.Client.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormBase : Form
    {
        internal V1Client _apiV1Client;
        internal Request _requestInfo;

        public FormBase()
        {
            InitializeComponent();

            _requestInfo = new Request();
        }

        public FormBase(V1Client v1Client)
            : this()
        {
            _apiV1Client = v1Client;
        }

        public FormBase(ServerConfiguration serverConfiguration)
            : this()
        {
            _apiV1Client = new V1Client(serverConfiguration);
        }


        internal void ShowResponse()
        {
            var formResponse = new FormErrorMessages(_requestInfo);
            formResponse.ShowDialog();

            formResponse.Dispose();
        }
    }
}
