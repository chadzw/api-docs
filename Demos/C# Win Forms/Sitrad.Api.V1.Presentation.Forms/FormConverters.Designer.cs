﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormConverters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvInstruments = new System.Windows.Forms.DataGridView();
            this.btnResponseInfos = new System.Windows.Forms.Button();
            this.btnGoToInstruments = new System.Windows.Forms.Button();
            this.btnGetConverters = new System.Windows.Forms.Button();
            this.cmbConverterStatus = new System.Windows.Forms.ComboBox();
            this.lblInstrumentStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstruments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvInstruments
            // 
            this.dgvInstruments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInstruments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstruments.Location = new System.Drawing.Point(12, 55);
            this.dgvInstruments.Name = "dgvInstruments";
            this.dgvInstruments.RowTemplate.Height = 25;
            this.dgvInstruments.Size = new System.Drawing.Size(1002, 488);
            this.dgvInstruments.TabIndex = 0;
            this.dgvInstruments.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstruments_CellDoubleClick);
            // 
            // btnResponseInfos
            // 
            this.btnResponseInfos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnResponseInfos.Location = new System.Drawing.Point(12, 555);
            this.btnResponseInfos.Name = "btnResponseInfos";
            this.btnResponseInfos.Size = new System.Drawing.Size(107, 23);
            this.btnResponseInfos.TabIndex = 1;
            this.btnResponseInfos.Text = "Response Info";
            this.btnResponseInfos.UseVisualStyleBackColor = true;
            this.btnResponseInfos.Click += new System.EventHandler(this.btnResponseInfos_Click);
            // 
            // btnGoToInstruments
            // 
            this.btnGoToInstruments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoToInstruments.Enabled = false;
            this.btnGoToInstruments.Location = new System.Drawing.Point(835, 555);
            this.btnGoToInstruments.Name = "btnGoToInstruments";
            this.btnGoToInstruments.Size = new System.Drawing.Size(179, 23);
            this.btnGoToInstruments.TabIndex = 2;
            this.btnGoToInstruments.Text = "Converter instruments...";
            this.btnGoToInstruments.UseVisualStyleBackColor = true;
            this.btnGoToInstruments.Click += new System.EventHandler(this.lblGoToInstrument_Click);
            // 
            // btnGetConverters
            // 
            this.btnGetConverters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetConverters.Location = new System.Drawing.Point(884, 16);
            this.btnGetConverters.Name = "btnGetConverters";
            this.btnGetConverters.Size = new System.Drawing.Size(130, 23);
            this.btnGetConverters.TabIndex = 13;
            this.btnGetConverters.Text = "Get Converters";
            this.btnGetConverters.UseVisualStyleBackColor = true;
            this.btnGetConverters.Click += new System.EventHandler(this.btnGetConverters_Click);
            // 
            // cmbConverterStatus
            // 
            this.cmbConverterStatus.FormattingEnabled = true;
            this.cmbConverterStatus.Location = new System.Drawing.Point(452, 16);
            this.cmbConverterStatus.Name = "cmbConverterStatus";
            this.cmbConverterStatus.Size = new System.Drawing.Size(364, 23);
            this.cmbConverterStatus.TabIndex = 12;
            // 
            // lblInstrumentStatus
            // 
            this.lblInstrumentStatus.AutoSize = true;
            this.lblInstrumentStatus.Location = new System.Drawing.Point(346, 20);
            this.lblInstrumentStatus.Name = "lblInstrumentStatus";
            this.lblInstrumentStatus.Size = new System.Drawing.Size(94, 15);
            this.lblInstrumentStatus.TabIndex = 11;
            this.lblInstrumentStatus.Text = "Converter Status";
            // 
            // FormConverters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 590);
            this.Controls.Add(this.btnGetConverters);
            this.Controls.Add(this.cmbConverterStatus);
            this.Controls.Add(this.lblInstrumentStatus);
            this.Controls.Add(this.btnGoToInstruments);
            this.Controls.Add(this.btnResponseInfos);
            this.Controls.Add(this.dgvInstruments);
            this.Name = "FormConverters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Converters";
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstruments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInstruments;
        private System.Windows.Forms.Button btnResponseInfos;
        private System.Windows.Forms.Button lblGoToInstrument;
        private System.Windows.Forms.Button btnGoToInstruments;
        private System.Windows.Forms.Button btnGetConverters;
        private System.Windows.Forms.ComboBox cmbConverterStatus;
        private System.Windows.Forms.Label lblInstrumentStatus;
    }
}