﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvValues = new System.Windows.Forms.DataGridView();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnGetAlarms = new System.Windows.Forms.Button();
            this.btnUpdateInstrument = new System.Windows.Forms.Button();
            this.cmbInstrumentStatus = new System.Windows.Forms.ComboBox();
            this.btnGetValues = new System.Windows.Forms.Button();
            this.btnGetFunctions = new System.Windows.Forms.Button();
            this.dgvFunctions = new System.Windows.Forms.DataGridView();
            this.lblValues = new System.Windows.Forms.Label();
            this.lblFuntions = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFunctions)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvValues
            // 
            this.dgvValues.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValues.Location = new System.Drawing.Point(12, 93);
            this.dgvValues.Name = "dgvValues";
            this.dgvValues.RowTemplate.Height = 25;
            this.dgvValues.Size = new System.Drawing.Size(1002, 179);
            this.dgvValues.TabIndex = 0;
            this.dgvValues.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstruments_CellDoubleClick);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(451, 13);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(317, 23);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(406, 19);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 15);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(131, 16);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 15);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Status";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(12, 16);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(17, 15);
            this.lblId.TabIndex = 6;
            this.lblId.Text = "Id";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(44, 13);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(68, 23);
            this.txtId.TabIndex = 5;
            // 
            // btnGetAlarms
            // 
            this.btnGetAlarms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetAlarms.Location = new System.Drawing.Point(897, 12);
            this.btnGetAlarms.Name = "btnGetAlarms";
            this.btnGetAlarms.Size = new System.Drawing.Size(117, 23);
            this.btnGetAlarms.TabIndex = 7;
            this.btnGetAlarms.Text = "Get alarms...";
            this.btnGetAlarms.UseVisualStyleBackColor = true;
            this.btnGetAlarms.Click += new System.EventHandler(this.btnGetAlarms_Click);
            // 
            // btnUpdateInstrument
            // 
            this.btnUpdateInstrument.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateInstrument.Location = new System.Drawing.Point(774, 12);
            this.btnUpdateInstrument.Name = "btnUpdateInstrument";
            this.btnUpdateInstrument.Size = new System.Drawing.Size(117, 23);
            this.btnUpdateInstrument.TabIndex = 8;
            this.btnUpdateInstrument.Text = "Update Instrument";
            this.btnUpdateInstrument.UseVisualStyleBackColor = true;
            this.btnUpdateInstrument.Click += new System.EventHandler(this.btnUpdateInstrument_Click);
            // 
            // cmbInstrumentStatus
            // 
            this.cmbInstrumentStatus.FormattingEnabled = true;
            this.cmbInstrumentStatus.Location = new System.Drawing.Point(176, 13);
            this.cmbInstrumentStatus.Name = "cmbInstrumentStatus";
            this.cmbInstrumentStatus.Size = new System.Drawing.Size(224, 23);
            this.cmbInstrumentStatus.TabIndex = 9;
            // 
            // btnGetValues
            // 
            this.btnGetValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetValues.Location = new System.Drawing.Point(897, 64);
            this.btnGetValues.Name = "btnGetValues";
            this.btnGetValues.Size = new System.Drawing.Size(117, 23);
            this.btnGetValues.TabIndex = 10;
            this.btnGetValues.Text = "Get values";
            this.btnGetValues.UseVisualStyleBackColor = true;
            this.btnGetValues.Click += new System.EventHandler(this.btnGetValues_Click);
            // 
            // btnGetFunctions
            // 
            this.btnGetFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetFunctions.Location = new System.Drawing.Point(897, 301);
            this.btnGetFunctions.Name = "btnGetFunctions";
            this.btnGetFunctions.Size = new System.Drawing.Size(117, 23);
            this.btnGetFunctions.TabIndex = 12;
            this.btnGetFunctions.Text = "Get functions";
            this.btnGetFunctions.UseVisualStyleBackColor = true;
            this.btnGetFunctions.Click += new System.EventHandler(this.btnGetProperties_Click);
            // 
            // dgvFunctions
            // 
            this.dgvFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFunctions.Location = new System.Drawing.Point(12, 330);
            this.dgvFunctions.Name = "dgvFunctions";
            this.dgvFunctions.RowTemplate.Height = 25;
            this.dgvFunctions.Size = new System.Drawing.Size(1002, 213);
            this.dgvFunctions.TabIndex = 11;
            this.dgvFunctions.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFunctions_CellDoubleClick);
            // 
            // lblValues
            // 
            this.lblValues.AutoSize = true;
            this.lblValues.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblValues.Location = new System.Drawing.Point(12, 72);
            this.lblValues.Name = "lblValues";
            this.lblValues.Size = new System.Drawing.Size(42, 15);
            this.lblValues.TabIndex = 13;
            this.lblValues.Text = "Values";
            // 
            // lblFuntions
            // 
            this.lblFuntions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFuntions.AutoSize = true;
            this.lblFuntions.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblFuntions.Location = new System.Drawing.Point(12, 309);
            this.lblFuntions.Name = "lblFuntions";
            this.lblFuntions.Size = new System.Drawing.Size(60, 15);
            this.lblFuntions.TabIndex = 14;
            this.lblFuntions.Text = "Functions";
            // 
            // FormInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 555);
            this.Controls.Add(this.lblFuntions);
            this.Controls.Add(this.lblValues);
            this.Controls.Add(this.btnGetFunctions);
            this.Controls.Add(this.dgvFunctions);
            this.Controls.Add(this.btnGetValues);
            this.Controls.Add(this.cmbInstrumentStatus);
            this.Controls.Add(this.btnUpdateInstrument);
            this.Controls.Add(this.btnGetAlarms);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.dgvValues);
            this.Name = "FormInstrument";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Instrument";
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFunctions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvValues;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button btnGetAlarms;
        private System.Windows.Forms.Button btnUpdateInstrument;
        private System.Windows.Forms.ComboBox cmbInstrumentStatus;
        private System.Windows.Forms.Button btnGetValues;
        private System.Windows.Forms.Button btnGetFunctions;
        private System.Windows.Forms.DataGridView dgvFunctions;
        private System.Windows.Forms.Label lblValues;
        private System.Windows.Forms.Label lblFuntions;
    }
}