﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.V1.Presentation.Forms.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormAlarms : FormBase
    {
        private List<Alarm> _alarms = new List<Alarm>();

        public FormAlarms(V1Client apiV1Client, Instrument instrument = null)
             : base(apiV1Client)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterParent;
            base.Text = $"Alarms - {_alarms.Count} Founded";

            dtpStartDate.Value = DateTime.Now.AddDays(-10);
            dtpEndDate.Value = DateTime.Now;

            dtpStartDate.Format = DateTimePickerFormat.Short;
            dtpStartDateTime.Format = DateTimePickerFormat.Time;
            dtpStartDateTime.ShowUpDown = true;

            dtpEndDate.Format = DateTimePickerFormat.Short;
            dtpEndDateTime.Format = DateTimePickerFormat.Time;
            dtpEndDateTime.ShowUpDown = true;

            if (instrument != null)
                txtInstrumentId.Text = instrument.Id.ToString();

            var items = Enum.GetValues(typeof(AlarmStatusEnum))
                .Cast<AlarmStatusEnum>()
                .Select(x => new ComboBoxItem((int)x, x.ToString()))
                .ToList();

            items.Insert(0, new ComboBoxItem(-1, string.Empty));

            cmbAlarmStatus.DisplayMember = "DisplayValue";
            cmbAlarmStatus.DataSource = items;

            // Initialize the DataGridView.
            dgvAlarms.AutoGenerateColumns = false;
            dgvAlarms.AutoSize = true;
            dgvAlarms.MultiSelect = false;
            dgvAlarms.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            foreach (var property in typeof(Alarm).GetProperties())
            {
                // Initialize and add a text box column.
                DataGridViewColumn column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = property.Name;
                column.Name = property.Name;
                dgvAlarms.Columns.Add(column);
            }

            FillAlarms();
        }

        private void FillAlarms()
        {
            var startDate = dtpStartDate.Value.Date.Add(dtpStartDateTime.Value.TimeOfDay);
            var endDate = dtpEndDate.Value.Date.Add(dtpEndDateTime.Value.TimeOfDay);
            var alarmStatus = ((ComboBoxItem)cmbAlarmStatus.SelectedItem).Value >= 0
                ? ((ComboBoxItem)cmbAlarmStatus.SelectedItem).Value
                : (int?)null;
            
            _alarms = !string.IsNullOrEmpty(txtInstrumentId.Text)
                ? _apiV1Client.GetInstrumentAlarms(
                    int.Parse(txtInstrumentId.Text),
                    startDate,
                    endDate,
                    alarmStatus,
                    out _requestInfo)
                : _apiV1Client.GetAlarms(
                    startDate,
                    endDate,
                    alarmStatus,
                    out _requestInfo);

            dgvAlarms.DataSource = _alarms;

            dgvAlarms.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvAlarms.Refresh();
        }

        private void dgvInstruments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            var alarm = (Alarm)dgvAlarms.Rows[e.RowIndex].DataBoundItem;

        }

        private void btnGetAlarms_Click(object sender, System.EventArgs e)
        {
           FillAlarms();
        }
    }
}
