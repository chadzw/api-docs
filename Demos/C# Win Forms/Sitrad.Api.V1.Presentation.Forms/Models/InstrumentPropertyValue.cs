﻿
namespace Sitrad.Api.V1.Presentation.Forms.Models
{
    public class InstrumentPropertyValueView
    {
        public string Code { get; set; }

        public string Date { get; set; }

        public object Value { get; set; }

        public string MeasurementUnity { get; set; }

        public MeasurementUnityEnum MeasurementUnityId { get; set; }

        public int DecimalPlaces { get; set; }

        public bool IsFailPayload { get; set; }
    }
}
