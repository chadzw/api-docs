﻿namespace Sitrad.Api.V1.Presentation.Forms.Models
{
    public class ComboBoxItem
    {
        public ComboBoxItem(int value, string displayValue)
        {
            Value = value;
            DisplayValue = displayValue;
        }

        public int Value { get; set; }
        public string DisplayValue { get; set; }
    }
}
