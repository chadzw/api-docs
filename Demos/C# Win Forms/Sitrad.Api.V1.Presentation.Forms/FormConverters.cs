﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.V1.Presentation.Forms.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormConverters : FormBase
    {
        private List<Converter> _instruments = new List<Converter>();

        public FormConverters(V1Client apiV1Client)
            : base(apiV1Client)
        {
            InitializeComponent();

            var items = Enum.GetValues(typeof(ConverterStatusEnum))
                .Cast<ConverterStatusEnum>()
                .Select(x => new ComboBoxItem((int)x, x.ToString()))
                .ToList();

            items.Insert(0, new ComboBoxItem(-1, string.Empty));

            cmbConverterStatus.DisplayMember = "DisplayValue";
            cmbConverterStatus.DataSource = items;

            dgvInstruments.AutoGenerateColumns = false;
            dgvInstruments.AutoSize = true;
            dgvInstruments.MultiSelect = false;
            dgvInstruments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            foreach (var property in typeof(Converter).GetProperties())
            {
                // Initialize and add a text box column.
                DataGridViewColumn column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = property.Name;
                column.Name = property.Name;
                dgvInstruments.Columns.Add(column);
            }

            dgvInstruments.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            FillConverters();
        }

        private void FillConverters()
        {
            var converterStatus = ((ComboBoxItem)cmbConverterStatus.SelectedItem).Value >= 0
                ? ((ComboBoxItem)cmbConverterStatus.SelectedItem).Value
                : (int?)null;

            _instruments = _apiV1Client.GetConverters(converterStatus,  out _requestInfo);

            dgvInstruments.DataSource = _instruments;
            dgvInstruments.Refresh();

            btnGoToInstruments.Enabled = _instruments.Any();

            if (_requestInfo.Errors.Any())
                ShowResponse();
        }

        private void dgvInstruments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            var converter = (Converter)dgvInstruments.Rows[e.RowIndex].DataBoundItem;

            OpenFormInstruments(converter);
        }

        private void btnResponseInfos_Click(object sender, System.EventArgs e)
        {
            ShowResponse();
        }

        private void lblGoToInstrument_Click(object sender, System.EventArgs e)
        {
            var rowSelected = dgvInstruments.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            var converter = (Converter)dgvInstruments.Rows[rowSelected].DataBoundItem;

            OpenFormInstruments(converter);
        }

        private void OpenFormInstruments(Converter converter)
        {
            var form = new FormInstruments(_apiV1Client, converter);
            form.ShowDialog();
            form.Dispose();
        }

        private void btnGetConverters_Click(object sender, EventArgs e)
        {
            FillConverters();
        }
    }
}
