﻿using System.Net;
using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.Client.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormPrincipal : FormBase
    {
        private ServerConfiguration _serverConfiguration;

        public FormPrincipal() 
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterScreen;

            _serverConfiguration = new ServerConfiguration();

            txtUri.Text = _serverConfiguration.SitradApiUrl;
            txtUser.Text = _serverConfiguration.User;
            txtPassword.Text = _serverConfiguration.Password;
            ckbIgnoreCertificateValidation.Checked = _serverConfiguration.IgnoreCertificateValidation;
        }

        private void btnTestConnection_Click(object sender, System.EventArgs e)
        {
            _serverConfiguration.SitradApiUrl = txtUri.Text;
            _serverConfiguration.User = txtUser.Text;
            _serverConfiguration.Password = txtPassword.Text;
            _serverConfiguration.IgnoreCertificateValidation = ckbIgnoreCertificateValidation.Checked;

            _apiV1Client = new V1Client(_serverConfiguration);

            _apiV1Client.GetInstruments(null, out _requestInfo);

            if (_requestInfo.StatusCode != HttpStatusCode.OK)
            {
                txtValidationResult.Text = "ERROR";
                ChangeBtnEnableStatus(false);
                ShowResponse();
            }
            else
            {
                txtValidationResult.Text = "OK";
                ChangeBtnEnableStatus(true);
            }
        }

        private void ChangeBtnEnableStatus(bool enableStatus)
        {
            btnGetAlarms.Enabled =
                btnGetConverters.Enabled =
                    btnGetInstruments.Enabled = enableStatus;

        }

        private void ckbIgnoreCertificateValidation_CheckedChanged(object sender, System.EventArgs e)
        {
            _serverConfiguration.IgnoreCertificateValidation = ckbIgnoreCertificateValidation.Checked;
        }

        private void btnGetInstruments_Click(object sender, System.EventArgs e)
        {
            var form = new FormInstruments(_apiV1Client);
            form.ShowDialog();
            form.Dispose();
        }

        private void btnGetAlarms_Click(object sender, System.EventArgs e)
        {
            var form = new FormAlarms(_apiV1Client);
            form.ShowDialog();
            form.Dispose();
        }

        private void btnGetConverters_Click(object sender, System.EventArgs e)
        {
            var form = new FormConverters(_apiV1Client);
            form.ShowDialog();
            form.Dispose();
        }
    }
}
