﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormAlarms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAlarms = new System.Windows.Forms.DataGridView();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.btnGetAlarms = new System.Windows.Forms.Button();
            this.lblInstrumentId = new System.Windows.Forms.Label();
            this.txtInstrumentId = new System.Windows.Forms.TextBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpStartDateTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDateTime = new System.Windows.Forms.DateTimePicker();
            this.cmbAlarmStatus = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarms)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAlarms
            // 
            this.dgvAlarms.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAlarms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlarms.Location = new System.Drawing.Point(12, 78);
            this.dgvAlarms.Name = "dgvAlarms";
            this.dgvAlarms.RowTemplate.Height = 25;
            this.dgvAlarms.Size = new System.Drawing.Size(1002, 465);
            this.dgvAlarms.TabIndex = 0;
            this.dgvAlarms.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstruments_CellDoubleClick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(179, 14);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 15);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Status";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(407, 14);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(58, 15);
            this.lblStartDate.TabIndex = 6;
            this.lblStartDate.Text = "Start Date";
            // 
            // btnGetAlarms
            // 
            this.btnGetAlarms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetAlarms.Location = new System.Drawing.Point(939, 49);
            this.btnGetAlarms.Name = "btnGetAlarms";
            this.btnGetAlarms.Size = new System.Drawing.Size(75, 23);
            this.btnGetAlarms.TabIndex = 7;
            this.btnGetAlarms.Text = "Get alarms";
            this.btnGetAlarms.UseVisualStyleBackColor = true;
            this.btnGetAlarms.Click += new System.EventHandler(this.btnGetAlarms_Click);
            // 
            // lblInstrumentId
            // 
            this.lblInstrumentId.AutoSize = true;
            this.lblInstrumentId.Location = new System.Drawing.Point(12, 14);
            this.lblInstrumentId.Name = "lblInstrumentId";
            this.lblInstrumentId.Size = new System.Drawing.Size(78, 15);
            this.lblInstrumentId.TabIndex = 9;
            this.lblInstrumentId.Text = "Instrument Id";
            // 
            // txtInstrumentId
            // 
            this.txtInstrumentId.Location = new System.Drawing.Point(98, 11);
            this.txtInstrumentId.Name = "txtInstrumentId";
            this.txtInstrumentId.Size = new System.Drawing.Size(75, 23);
            this.txtInstrumentId.TabIndex = 8;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(471, 11);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(129, 23);
            this.dtpStartDate.TabIndex = 10;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(770, 11);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(129, 23);
            this.dtpEndDate.TabIndex = 12;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(710, 17);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(54, 15);
            this.lblEndDate.TabIndex = 11;
            this.lblEndDate.Text = "End Date";
            // 
            // dtpStartDateTime
            // 
            this.dtpStartDateTime.Location = new System.Drawing.Point(606, 11);
            this.dtpStartDateTime.Name = "dtpStartDateTime";
            this.dtpStartDateTime.Size = new System.Drawing.Size(98, 23);
            this.dtpStartDateTime.TabIndex = 13;
            // 
            // dtpEndDateTime
            // 
            this.dtpEndDateTime.Location = new System.Drawing.Point(905, 11);
            this.dtpEndDateTime.Name = "dtpEndDateTime";
            this.dtpEndDateTime.Size = new System.Drawing.Size(109, 23);
            this.dtpEndDateTime.TabIndex = 14;
            // 
            // cmbAlarmStatus
            // 
            this.cmbAlarmStatus.FormattingEnabled = true;
            this.cmbAlarmStatus.Location = new System.Drawing.Point(224, 11);
            this.cmbAlarmStatus.Name = "cmbAlarmStatus";
            this.cmbAlarmStatus.Size = new System.Drawing.Size(177, 23);
            this.cmbAlarmStatus.TabIndex = 15;
            // 
            // FormAlarms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 555);
            this.Controls.Add(this.cmbAlarmStatus);
            this.Controls.Add(this.dtpEndDateTime);
            this.Controls.Add(this.dtpStartDateTime);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.lblInstrumentId);
            this.Controls.Add(this.txtInstrumentId);
            this.Controls.Add(this.btnGetAlarms);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.dgvAlarms);
            this.Name = "FormAlarms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alarms";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarms)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAlarms;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Button btnGetAlarms;
        private System.Windows.Forms.Label lblInstrumentId;
        private System.Windows.Forms.TextBox txtInstrumentId;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDateTime;
        private System.Windows.Forms.DateTimePicker dtpEndDateTime;
        private System.Windows.Forms.ComboBox cmbAlarmStatus;
    }
}