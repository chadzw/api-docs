﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormPrincipal
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.txtUri = new System.Windows.Forms.TextBox();
            this.lblUri = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.txtValidationResult = new System.Windows.Forms.Label();
            this.ckbIgnoreCertificateValidation = new System.Windows.Forms.CheckBox();
            this.btnGetInstruments = new System.Windows.Forms.Button();
            this.btnGetConverters = new System.Windows.Forms.Button();
            this.btnGetAlarms = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtUri
            // 
            this.txtUri.Location = new System.Drawing.Point(103, 134);
            this.txtUri.Name = "txtUri";
            this.txtUri.Size = new System.Drawing.Size(546, 23);
            this.txtUri.TabIndex = 0;
            // 
            // lblUri
            // 
            this.lblUri.AutoSize = true;
            this.lblUri.Location = new System.Drawing.Point(29, 137);
            this.lblUri.Name = "lblUri";
            this.lblUri.Size = new System.Drawing.Size(68, 15);
            this.lblUri.TabIndex = 1;
            this.lblUri.Text = "API address";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(333, 166);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(57, 15);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(396, 163);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(253, 23);
            this.txtPassword.TabIndex = 2;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(67, 166);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(30, 15);
            this.lblUser.TabIndex = 5;
            this.lblUser.Text = "User";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(103, 163);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(224, 23);
            this.txtUser.TabIndex = 4;
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Location = new System.Drawing.Point(665, 134);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(99, 55);
            this.btnTestConnection.TabIndex = 8;
            this.btnTestConnection.Text = "Test \r\nconnection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // txtValidationResult
            // 
            this.txtValidationResult.Location = new System.Drawing.Point(665, 192);
            this.txtValidationResult.Name = "txtValidationResult";
            this.txtValidationResult.Size = new System.Drawing.Size(61, 23);
            this.txtValidationResult.TabIndex = 9;
            // 
            // ckbIgnoreCertificateValidation
            // 
            this.ckbIgnoreCertificateValidation.AutoSize = true;
            this.ckbIgnoreCertificateValidation.Location = new System.Drawing.Point(103, 191);
            this.ckbIgnoreCertificateValidation.Name = "ckbIgnoreCertificateValidation";
            this.ckbIgnoreCertificateValidation.Size = new System.Drawing.Size(172, 19);
            this.ckbIgnoreCertificateValidation.TabIndex = 10;
            this.ckbIgnoreCertificateValidation.Text = "Ignore certficate validation?";
            this.ckbIgnoreCertificateValidation.UseVisualStyleBackColor = true;
            this.ckbIgnoreCertificateValidation.CheckedChanged += new System.EventHandler(this.ckbIgnoreCertificateValidation_CheckedChanged);
            // 
            // btnGetInstruments
            // 
            this.btnGetInstruments.Enabled = false;
            this.btnGetInstruments.Location = new System.Drawing.Point(36, 263);
            this.btnGetInstruments.Name = "btnGetInstruments";
            this.btnGetInstruments.Size = new System.Drawing.Size(728, 23);
            this.btnGetInstruments.TabIndex = 11;
            this.btnGetInstruments.Text = "Get Instruments";
            this.btnGetInstruments.UseVisualStyleBackColor = true;
            this.btnGetInstruments.Click += new System.EventHandler(this.btnGetInstruments_Click);
            // 
            // btnGetConverters
            // 
            this.btnGetConverters.Enabled = false;
            this.btnGetConverters.Location = new System.Drawing.Point(36, 234);
            this.btnGetConverters.Name = "btnGetConverters";
            this.btnGetConverters.Size = new System.Drawing.Size(728, 23);
            this.btnGetConverters.TabIndex = 12;
            this.btnGetConverters.Text = "Get Converters";
            this.btnGetConverters.UseVisualStyleBackColor = true;
            this.btnGetConverters.Click += new System.EventHandler(this.btnGetConverters_Click);
            // 
            // btnGetAlarms
            // 
            this.btnGetAlarms.Enabled = false;
            this.btnGetAlarms.Location = new System.Drawing.Point(36, 292);
            this.btnGetAlarms.Name = "btnGetAlarms";
            this.btnGetAlarms.Size = new System.Drawing.Size(728, 23);
            this.btnGetAlarms.TabIndex = 13;
            this.btnGetAlarms.Text = "Get Alarms";
            this.btnGetAlarms.UseVisualStyleBackColor = true;
            this.btnGetAlarms.Click += new System.EventHandler(this.btnGetAlarms_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Sitrad.Api.V1.Presentation.Forms.Resource.logo;
            this.pictureBox1.Location = new System.Drawing.Point(630, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 90);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(228, 16);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(344, 90);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 326);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnGetAlarms);
            this.Controls.Add(this.btnGetConverters);
            this.Controls.Add(this.btnGetInstruments);
            this.Controls.Add(this.ckbIgnoreCertificateValidation);
            this.Controls.Add(this.txtValidationResult);
            this.Controls.Add(this.btnTestConnection);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.lblUri);
            this.Controls.Add(this.txtUri);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPrincipal";
            this.ShowInTaskbar = true;
            this.Text = "Sitrad API V1 Demo";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUri;
        private System.Windows.Forms.Label lblUri;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.Label txtValidationResult;
        private System.Windows.Forms.CheckBox ckbIgnoreCertificateValidation;
        private System.Windows.Forms.Button btnGetInstruments;
        private System.Windows.Forms.Button btnGetConverters;
        private System.Windows.Forms.Button btnGetAlarms;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

