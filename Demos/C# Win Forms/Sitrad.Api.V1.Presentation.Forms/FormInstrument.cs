﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.V1.Bodies;
using Sitrad.Api.V1.Presentation.Forms.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormInstrument : FormBase
    {
        private Instrument _instrument;
        private List<InstrumentProperty> _instrumentProperties;
        private List<InstrumentFunction> _instrumentFunctions;
        private List<InstrumentPropertyValueView> _instrumentPropertiesValueViews;

        public FormInstrument(V1Client apiV1Client, Instrument instrument)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterParent;
            base.Text = $"{instrument.Id} - {instrument.Name}";

            _apiV1Client = apiV1Client;
            _instrument = instrument;

            txtId.Text = instrument.Id.ToString();
            txtName.Text = instrument.Name;

            var items = Enum.GetValues(typeof(InstrumentStatusEnum))
                .Cast<InstrumentStatusEnum>()
                .Select(x => new ComboBoxItem((int)x, x.ToString()))
                .ToList();

            items.Insert(0, new ComboBoxItem(-1, string.Empty));

            cmbInstrumentStatus.DisplayMember = "DisplayValue";
            cmbInstrumentStatus.DataSource = items;
            cmbInstrumentStatus.SelectedItem = items.First(x => x.Value == (int)instrument.StatusEnumId);

            // Initialize the DataGridView.
            dgvValues.AutoGenerateColumns = false;
            dgvValues.AutoSize = false;
            dgvValues.MultiSelect = false;
            dgvValues.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            foreach (var property in typeof(InstrumentPropertyValueView).GetProperties())
            {
                // Initialize and add a text box column.
                DataGridViewColumn column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = property.Name;
                column.Name = property.Name;
                dgvValues.Columns.Add(column);
            }

            dgvValues.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            // Initialize the DataGridView.
            dgvFunctions.AutoGenerateColumns = false;
            dgvFunctions.AutoSize = false;
            dgvFunctions.MultiSelect = false;
            dgvFunctions.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            foreach (var property in typeof(InstrumentFunction).GetProperties())
            {
                // Initialize and add a text box column.
                DataGridViewColumn column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = property.Name;
                column.Name = property.Name;
                dgvFunctions.Columns.Add(column);
            }

            dgvFunctions.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;


        }

        private void FillValues()
        {
            var instrumentId = int.Parse(txtId.Text);

            _instrumentProperties = _apiV1Client.GetInstrumentValues(instrumentId, null, null, null, out _requestInfo);

            if (_requestInfo.Errors.Any())
            {
                ShowResponse();
                return;
            }

            _instrumentPropertiesValueViews = new List<InstrumentPropertyValueView>();

            foreach (var instrumentProperty in _instrumentProperties)
            {
                var instrumentPropertyValue = instrumentProperty.InstrumentPropertyValues.FirstOrDefault();

                var instrumentPropertyValueView = new InstrumentPropertyValueView();
                instrumentPropertyValueView.Code = instrumentProperty.Code;

                if (instrumentPropertyValue != null)
                {
                    instrumentPropertyValueView.Value = instrumentPropertyValue.Value;
                    instrumentPropertyValueView.Date = instrumentPropertyValue.Date;
                    instrumentPropertyValueView.DecimalPlaces = instrumentPropertyValue.DecimalPlaces;
                    instrumentPropertyValueView.IsFailPayload = instrumentPropertyValue.IsFailPayload;
                    instrumentPropertyValueView.MeasurementUnityId = instrumentPropertyValue.MeasurementUnityId;
                    instrumentPropertyValueView.MeasurementUnity = instrumentPropertyValue.MeasurementUnity;
                }

                _instrumentPropertiesValueViews.Add(instrumentPropertyValueView);
            }

            dgvValues.DataSource = _instrumentPropertiesValueViews;
           // dgvValues.Refresh();
         
        }

        private void FillFunctions()
        {
            var instrumentId = int.Parse(txtId.Text);

            _instrumentFunctions = _apiV1Client.GetInstrumentFunctions(instrumentId, null, out _requestInfo);

            if (_requestInfo.Errors.Any())
            {
                ShowResponse();
                return;
            }
            
            dgvFunctions.DataSource = _instrumentFunctions;
            //dgvFunctions.Refresh();
        }


        private void dgvInstruments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var instrument = (Instrument)dgvValues.Rows[e.RowIndex].DataBoundItem;
        }

        private void btnGetAlarms_Click(object sender, System.EventArgs e)
        {
            var form = new FormAlarms(_apiV1Client, _instrument);
            form.ShowDialog();
        }

        private void btnUpdateInstrument_Click(object sender, System.EventArgs e)
        {
            var putInstrument = new PutInstrument
            {
                Name = txtName.Text.Trim(),
                Status = ((InstrumentStatusEnum)((ComboBoxItem)cmbInstrumentStatus.SelectedItem).Value).ToString()
            };

            var result = _apiV1Client.UpdateInstrument(_instrument.Id, putInstrument, out _requestInfo);

            if (!result)
                ShowResponse();
        }

        private void btnGetValues_Click(object sender, EventArgs e)
        {
           FillValues();
        }

        private void btnGetProperties_Click(object sender, EventArgs e)
        {
            FillFunctions();
        }

        private void dgvFunctions_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            var instrument = (Instrument)dgvFunctions.Rows[e.RowIndex].DataBoundItem;
        }
    }
}
