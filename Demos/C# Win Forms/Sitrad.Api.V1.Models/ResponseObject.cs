﻿using System.Net;
using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Response with single alarm object
    /// </summary>
    /// <typeparam name="TResult">Object with the interface implementation</typeparam>
    public  class ResponseObject<TResult> : BaseResponse
        where TResult : IResult
    {
        protected ResponseObject()
        {
            StatusCode = (HttpStatusCodeEnum) HttpStatusCode.OK;
        }

        [JsonProperty("result")]
        public TResult Result { get; set; }
    }
}