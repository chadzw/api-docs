﻿using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Response body for cases where needs to be sent a list of objects of TResult type.
    /// </summary>
    /// <typeparam name="TResult">Objects with the interface implementation</typeparam>
    public class ResponseObjects<TResult> : BaseResponse
        where TResult : IResult
    {
        protected ResponseObjects()
        {
            StatusCode = (HttpStatusCodeEnum) HttpStatusCode.OK;
        }

        [JsonProperty("results")]
        public List<TResult> Result { get; set; }

        /// <summary>
        /// Quantity of results objects
        /// </summary>
        [JsonProperty("resultsQty")]
        public int ResultsQty { get; set; }
    }
}