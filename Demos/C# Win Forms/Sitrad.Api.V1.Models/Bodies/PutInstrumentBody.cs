﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1.Bodies
{
    /// <summary>
    /// Object to update a instrument
    /// </summary>
    public class PutInstrument : IBody
    {
        // TODO ver como esclarecer que é o enum InstrumentStatusEnum
        /// <summary>
        /// Instrument status: active, inactive, convertermaintenance, maintenance
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }


        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
