﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;
using Sitrad.Api.V1;

namespace FG.Sitrad.Api.V1
{
    public class AlarmInhibition : IResult
    {
        /// <summary>
        /// Unique identifier of alarm inhibition
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Unique identifier of instrument
        /// </summary>
        [JsonProperty("instrumentId")]
        public int InstrumentId { get; set; }

        /// <summary>
        /// Infos about the schedule parameters
        /// </summary>
        [JsonProperty("schedule")]
        public AlarmInhibitionSchedule Schedule { get; set; }

        /// <summary>
        /// Array of alarms codes inhibited
        /// </summary>
        [JsonProperty("alarmCodes")]
        public string[] AlarmCodes { get; set; }
    }
}
