﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class Preset : IResult
    {
        /// <summary>
        /// Unique identifier of alarm
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Preset name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("modelId")]
        public int ModelId { get; set; }

        [JsonProperty("modelVersion")]
        public int ModelVersion { get; set; }

        [JsonProperty("creationDate")]
        public string CreationDate { get; set; }
    }
}
