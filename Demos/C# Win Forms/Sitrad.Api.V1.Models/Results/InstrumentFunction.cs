﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class InstrumentFunction : IResult
    {
        /// <summary>
        /// Unique identifier of function"
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// Unique identifier of value
        /// </summary>
        [JsonProperty("valueCode")]
        public string ValueCode { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("valueType")]
        public DataTypeEnum ValueType { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("extraDescription")]
        public string ExtraDescription { get; set; }

        [JsonProperty("minValue")]
        public double? MinValue { get; set; }

        [JsonProperty("maxValue")]
        public double? MaxValue { get; set; }

        [JsonProperty("rangeValues")]
        public double[] RangeValues { get; set; }

        [JsonProperty("defaultValue")]
        public double DefaultValue { get; set; }

        [JsonProperty("decimalPlaces")]
        public int DecimalPlaces { get; set; }

        [JsonProperty("measurementUnityId")]
        public MeasurementUnityEnum MeasurementUnityId { get; set; }

        [JsonProperty("measurementUnity")]
        public string MeasurementUnity { get; set; }

        [JsonProperty("groupCode")]
        public int? GroupCode { get; set; }
    }



    
    public enum DataTypeEnum
    {
        [EnumMember] String,
        [EnumMember] Integer,
        [EnumMember] Boolean,
        [EnumMember] Double,
        [EnumMember] DateTime,
        [EnumMember] Enumerator,
        [EnumMember] Binary,
        [EnumMember] TimeSpan,
        [EnumMember] BitArray,
        [EnumMember] Byte,
        [EnumMember] InstrumentProcessStatus,
        [EnumMember] Undefined = 0xFF
    }
}
