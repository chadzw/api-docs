﻿namespace Sitrad.Api.V1
{
    public enum AlarmInhibitionTypeEnum
    {
        None = 0,
        Defrost = 1,
        Manual = 2,
        Schedule = 3,
        Delay = 4
    }
}
