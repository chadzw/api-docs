﻿namespace Sitrad.Api.V1
{
    public enum ConverterStatusEnum
    {
        inactive = 0,
        active = 1,
        maintenance = 2
    }
}