﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class FunctionGroup : IResult
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("code")]
        public bool Enable { get; set; }

        [JsonProperty("displayIndex")]
        public int DisplayIndex { get; set; }
    }
}
