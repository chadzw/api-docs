﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class Instrument : IResult
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("converterId")]
        public int ConverterId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public int Address { get; set; }

        [JsonProperty("statusId")]
        public InstrumentStatusEnum StatusEnumId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("modelId")]
        public int ModelId { get; set; }

        [JsonProperty("modelVersion")]
        public int ModelVersion { get; set; }

        [JsonProperty("isAlarmsManuallyInhibited")]
        public bool IsAlarmsManuallyInhibited { get; set; }
    }
}
