﻿namespace Sitrad.Api.V1
{
    public enum ConverterTypeEnum
    {
        usb = 0,
        serial = 1,
        tcp485 = 2,
        tcp485WiFi = 3,
        tcp485WiFiLog = 4,
        embeddedSerial = 5
    }
}
