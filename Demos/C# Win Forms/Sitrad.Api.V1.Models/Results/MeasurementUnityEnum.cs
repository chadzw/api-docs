﻿using System.Runtime.Serialization;

namespace Sitrad.Api.V1
{
    public enum MeasurementUnityEnum
    {
        /// <summary>
        /// Undefined measurement unit type
        /// </summary>
        [EnumMember]
        NONE = 0,

        /// <summary>
        /// Celsius Temperature unit
        /// </summary>
        [EnumMember]
        CELSIUS = 1,

        /// <summary>
        /// Farenheit Temperature unit
        /// </summary>
        [EnumMember]
        FAHRENHEIT = 2,

        /// <summary>
        /// Kelvin Temperature unit
        /// </summary>
        [EnumMember]
        KELVIN = 3,

        /// <summary>
        /// Atmosphere Pressure unit
        /// </summary>
        [EnumMember]
        ATMOSPHERE = 4,

        /// <summary>
        /// Bar Pressure unit
        /// </summary>
        [EnumMember]
        BAR = 5,

        /// <summary>
        /// Centimeters of Mercury Pressure unit
        /// </summary>
        [EnumMember]
        MILLIMITER_OF_MERCURY = 6,

        /// <summary>
        /// Centimeters of Mercury Pressure unit
        /// </summary>
        [EnumMember]
        CENTIMETER_OF_MERCURY = 7,

        /// <summary>
        /// Inches of Mercury Pressure unit
        /// </summary>
        [EnumMember]
        INCHE_OF_MERCURY = 8,

        /// <summary>
        /// Kilo by Square Centimeters Pressure unit
        /// </summary>
        [EnumMember]
        KILO_BY_SQUARE_CENTIMETERS = 9,

        /// <summary>
        /// Ton by Square Inches Pressure unit
        /// </summary>
        [EnumMember]
        TON_BY_SQUARE_INCHES = 10,

        /// <summary>
        /// Pascal Pressure unit
        /// </summary>
        [EnumMember]
        PASCAL = 11,

        /// <summary>
        /// PSI Pressure unit
        /// </summary>
        [EnumMember]
        PSI = 12,

        /// <summary>
        /// Inches of Water Pressure unit
        /// </summary>
        [EnumMember]
        INCHES_OF_WATER = 13,

        /// <summary>
        /// Humidity unit
        /// </summary>
        [EnumMember]
        HUMIDITY = 14,

        /// <summary>
        /// Cubic Centimenter Fluid unit
        /// </summary>
        [EnumMember]
        CUBIC_CENTIMETER = 15,

        /// <summary>
        /// Cubic Inch Fluid unit
        /// </summary>
        [EnumMember]
        CUBIC_INCH = 16,

        /// <summary>
        /// Cubic Meter Fluid unit
        /// </summary>
        [EnumMember]
        CUBIC_METER = 17,

        /// <summary>
        /// Liter Fluid unit
        /// </summary>
        [EnumMember]
        LITER = 18,

        /// <summary>
        /// Parts per million Fluid unit
        /// </summary>
        [EnumMember]
        PPM = 19,

        /// <summary>
        /// Volt Power unit
        /// </summary>
        [EnumMember]
        VOLT = 20,

        /// <summary>
        /// Volt Power of direct current unit
        /// </summary>
        [EnumMember]
        VOLT_DC = 21,

        /// <summary>
        /// Volt Power in multiple of 10
        /// </summary>
        [EnumMember]
        VOLT_MULTIPLE_10 = 22,

        /// <summary>
        /// Volt Power unit
        /// </summary>
        [EnumMember]
        HERTZ = 23,

        /// <summary>
        /// Amper Power unit
        /// </summary>
        [EnumMember]
        AMPER = 24,

        /// <summary>
        /// Watt Power unit
        /// </summary>
        [EnumMember]
        WATT = 25,

        /// <summary>
        /// Horse Power unit
        /// </summary>
        [EnumMember]
        HP = 26,

        /// <summary>
        /// Gram Mass unit
        /// </summary>
        [EnumMember]
        GRAM = 27,

        /// <summary>
        /// Kilo Mass unit
        /// </summary>
        [EnumMember]
        KILO = 28,

        /// <summary>
        /// Ton Mass unit
        /// </summary>
        [EnumMember]
        TON = 29,

        /// <summary>
        /// Percentage unit
        /// </summary>
        [EnumMember]
        PERCENTAGE = 30,

        /// <summary>
        /// First Watch Bubble Level unit
        /// </summary>
        [EnumMember]
        FIRST_WATCH_BUBBLE_LEVEL = 31,

        /// <summary>
        /// First Watch Humidity unit
        /// </summary>
        [EnumMember]
        FIRST_WATCH_HUMIDITY = 32,

        /// <summary>
        /// Milliseconds unit
        /// </summary>
        [EnumMember]
        MILLISECOND = 33,

        /// <summary>
        /// Seconds unit
        /// </summary>
        [EnumMember]
        SECOND = 34,

        /// <summary>
        /// Minutes unit
        /// </summary>
        [EnumMember]
        MINUTE = 35,

        /// <summary>
        /// Hours unit
        /// </summary>
        [EnumMember]
        HOUR = 36,

        /// <summary>
        /// Seconds in multiples of 10 seconds
        /// </summary>
        [EnumMember]
        MILLISECOND_MULTIPLE_10 = 37,

        /// <summary>
        /// Seconds in multiples of 10 seconds
        /// </summary>
        [EnumMember]
        SECOND_MULTIPLE_10 = 38,

        /// <summary>
        /// Minutes in multiples of 10 minutes
        /// </summary>
        [EnumMember]
        MINUTE_MULTIPLE_10 = 39,

        /// <summary>
        /// Hours in multiples of 10 hours
        /// </summary>
        [EnumMember]
        HOUR_MULTIPLE_10 = 40,

        /// <summary>
        /// kWatt Power unit
        /// </summary>
        [EnumMember]
        KWATT = 41,

        /// <summary>
        /// Kilogram per meter cubed
        /// </summary>
        [EnumMember]
        KG_PER_M3 = 42,

        /// <summary>
        /// Joule per kilogram kelvin (SI)
        /// </summary>
        [EnumMember]
        J_PER_KG_K = 43,

        /// <summary>
        /// Liter per hour
        /// </summary>
        [EnumMember]
        LITER_PER_HOUR = 44,

        /// <summary>
        /// pH
        /// </summary>
        [EnumMember]
        PH = 45,

        /// <summary>
        /// Gramas por metro cúbico
        /// </summary>
        [EnumMember]
        G_PER_M3 = 46,

        /// <summary>
        /// Miligramas por metro cúbico
        /// </summary>
        [EnumMember]
        MILLI_G_PER_M3 = 47,

        /// <summary>
        /// Microgramas por metro cúbico
        /// </summary>
        [EnumMember]
        MICRO_G_PER_M3 = 48,

        /// <summary>
        /// Microsiemens
        /// </summary>
        [EnumMember]
        MICROSIEMENS = 49,

        /// <summary>
        /// Metros de coluna d'água
        /// </summary>
        [EnumMember]
        METERS_OF_WATER = 50,

        [EnumMember] RESERVED_1 = 51,

        [EnumMember] RESERVED_2 = 52,

        [EnumMember] RESERVED_3 = 53,

        /// <summary>
        /// Volt ampere reactive unit
        /// </summary>
        [EnumMember]
        VOLT_AMPERE_REACTIVE = 54,

        /// <summary>
        /// Volt ampere unit
        /// </summary>
        [EnumMember]
        VOLT_AMPERE = 55,

        /// <summary>
        /// Watt hour unit
        /// </summary>
        [EnumMember]
        WATT_HOUR = 56,

        /// <summary>
        /// Reactive Energy unit
        /// </summary>
        [EnumMember]
        REACTIVE_ENERGY = 57,

        /// <summary>
        /// Volt ampere hour unit
        /// </summary>
        [EnumMember]
        VOLT_AMPERE_HOUR = 58,

        /// <summary>
        /// Kilo Volt Ampere
        /// </summary>
        [EnumMember]
        KVOLT_AMPERE = 59,

        /// <summary>
        /// Mega Watt
        /// </summary>
        [EnumMember]
        MWATT = 60,

        /// <summary>
        /// Mega Volt Ampere
        /// </summary>
        [EnumMember]
        MVOLT_AMPERE = 61,

        /// <summary>
        /// kilo volt ampere relativo
        /// </summary>
        [EnumMember]
        KVOLT_AMPERE_REACTIVE = 62,

        /// <summary>
        /// Mega volt ampere relativo
        /// </summary>
        [EnumMember]
        MVOLT_AMPERE_REACTIVE = 63,

        /// <summary>
        /// kilo volt ampere hora
        /// </summary>
        [EnumMember]
        KVOLT_AMPERE_HOUR = 64,

        /// <summary>
        /// Mega volt ampere hora
        /// </summary>
        [EnumMember]
        MVOLT_AMPERE_HOUR = 65,

        /// <summary>
        /// kilo watt hora
        /// </summary>
        [EnumMember]
        KWATT_HOUR = 66,

        /// <summary>
        /// Mega watt hora
        /// </summary>
        [EnumMember]
        MWATT_HOUR = 67,

        /// <summary>
        /// Fator de Potência.
        /// Não é bem uma unidade de medida mas está no enum para facilitar o tratamento no gráfico.
        /// </summary>
        [EnumMember]
        POWER_FACTOR = 68,

        /// <summary>
        /// Metros
        /// </summary>
        [EnumMember]
        METER = 69,


        /// <summary>
        /// Yes or No. One or Zero.
        /// </summary>
        [EnumMember]
        YES_NO = 200,

        /// <summary>
        /// On or Off. One or Zero.
        /// </summary>
        [EnumMember]
        ON_OFF = 201,

        /// <summary>
        /// Refrigeration or Defrost. Zero or One.
        /// </summary>
        [EnumMember]
        REFRIG_DEFROST = 202,

        /// <summary>
        /// Refrigeration or Heating. Zero or One.
        /// </summary>
        [EnumMember]
        REFRIG_HEAT = 203,

        /// <summary>
        /// Dehumidification or Humidification. Zero or One.
        /// </summary>
        [EnumMember]
        DEHUMID_HUMID = 204,

        /// <summary>
        /// Seconds or Minutes. Zero or One.
        /// </summary>
        [EnumMember]
        SEC_MIN = 205,

        /// <summary>
        /// Minutes or Hours. Zero or One.
        /// </summary>
        [EnumMember]
        MIN_HOUR = 206,

        /// <summary>
        /// Hour and minute value.
        /// hh:mm (23:50 == 1450)
        /// </summary>
        [EnumMember]
        HOUR_AND_MINUTE = 207,

        /// <summary>
        /// Hour and minute value.
        /// hh:mm (23:50 == 145)
        /// </summary>
        [EnumMember]
        HOUR_AND_MINUTE_10 = 208,


        [EnumMember] UNDEFINED = 0xFFFF
    }
}
