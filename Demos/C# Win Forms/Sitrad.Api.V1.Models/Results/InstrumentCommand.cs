﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class InstrumentCommand : IResult
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("extraDescription")]
        public string ExtraDescription { get; set; }

        [JsonProperty("minValue")]
        public double MinValue { get; set; }

        [JsonProperty("maxValue")]
        public double MaxValue { get; set; }

        [JsonProperty("defaultValue")]
        public double DefaultValue { get; set; }

        [JsonProperty("decimalPlaces")]
        public int DecimalPlaces { get; set; }

        [JsonProperty("displayIndex")]
        public int DisplayIndex { get; set; }

        [JsonProperty("groupCode")]
        public int? GroupCode { get; set; }

        [JsonProperty("groupLabel")]
        public string GroupLabel { get; set; }

        [JsonProperty("groupExhibitionIndex")]
        public int? GroupExhibitionIndex { get; set; }
    }
}
