﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class AlarmConfiguration : IResult
    {
        [JsonProperty("alarmCode")]
        public string AlarmCode { get; set; }

        [JsonProperty("alarmCode")]
        public int? FunctionGroup { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("isVirtual")]
        public bool IsVirtual { get; set; }

        [JsonProperty("delaySeconds")]
        public int DelayTime { get; set; }

        [JsonProperty("minLimit")] 
        public AlarmConfigurationValues MinValue { get; set; }

        [JsonProperty("maxLimit")] 
        public AlarmConfigurationValues MaxValue { get; set; }

        [JsonProperty("currentValue")] 
        public AlarmConfigurationValues CurrentValue { get; set; }
    }
}