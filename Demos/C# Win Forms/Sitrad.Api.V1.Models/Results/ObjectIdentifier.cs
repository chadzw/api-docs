﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class ObjectIdentifier : IResult
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
