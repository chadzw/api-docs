﻿namespace Sitrad.Api.V1
{
    public enum InstrumentStatusEnum
    {
        inactive = 0,
        active = 1,
        convertermaintenance = 2,
        maintenance = 3
    }
}
