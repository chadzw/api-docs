﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class Alarm : IResult
    {
        /// <summary>
        /// Unique identifier of alarm
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Unique identifier of instrument
        /// </summary>
        [JsonProperty("instrumentId")]
        public int InstrumentId { get; set; }
        
        /// <summary>
        /// Code to identifier the alarm
        /// </summary>
        [JsonProperty("code")]
        public string AlarmCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("isRecognized")]
        public bool IsRecognized { get; set; }

        [JsonProperty("isFinalized")]
        public bool IsFinalized { get; set; }

        [JsonProperty("isInhibited")]
        public bool IsInhibited { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("recognizedDate")]
        public string RecognizedDate { get; set; }

        [JsonProperty("recognizedUserId")]
        public int? RecognizedUserId { get; set; }

        [JsonProperty("recognizedUserName")]
        public string RecognizedUserName { get; set; }

        [JsonProperty("inhibitionType")]
        public string InhibitionType { get; set; }

        [JsonProperty("inhibitionTypeId")]
        public AlarmInhibitionTypeEnum InhibitionTypeEnumId { get; set; }

        [JsonProperty("delayEndDate")]
        public string DelayEndDate { get; set; }
    }
}
