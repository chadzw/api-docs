# Overview
APIs (Application Programming Interface) are a type of “bridge” that connect applications. They provide the integration between systems developed in totally different languages ​​in an agile and secure way.

The [Sitrad Pro](https://www.sitrad.com/sitrad-pro)  API is available through a web service, based on the REST standard with data exchange in JSON format, and it is accessible on the machine where the Sitrad service is installed.\
Through it, it is possible for an application to ask Sitrad Pro for the current temperature of an instrument, its temperature history, alarm information and much more. It is also possible to use the feature, for example, to modify the control temperature (setpoint) of an instrument.

All the integration possibilities between the applications are available for consult through a documentation, being one of the ways to access it in the address of our demo server at https://fgserver-pro.sitrad.com/api/docs *(The browser can warn you that the site's certificate is invalid because it is a self-signed certificate. But you can accept it and continue browsing.)*.

# Project Folders
**Demos** - Client application demos.\
**Documents** - API documents that can help you to configure and use Sitrad Pro API.\
**Postman collections** - API requests previously configured to test with Postman software.\
**SSL Certificate** - Sitrad Pro default self-signed https certificate.